﻿using UnityEngine;
using System.Collections;

public class Enemies : MonoBehaviour {
    public bool Looker;
    public Animator animator;
    private Rigidbody rigidbdy;


    void start()
    {
        animator = GetComponent<Animator>();
        rigidbdy = GetComponent<Rigidbody>();
    }
    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag == "Player")
        {
            animator.SetTrigger("Damaged");
            Destroy(gameObject);
        }
    }
    }