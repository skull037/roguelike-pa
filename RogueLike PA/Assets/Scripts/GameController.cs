﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameController : MonoBehaviour {
    public static GameController Instance;
    public GameObject Menu;
    public GameObject Load;
    public GameObject Diffi;
    public bool ALERT;
    public AudioClip alertsound;
    public bool Firstly;
    public GameObject AlertOff;
    public GameObject AlertOn;
    public GameObject AlertCaution;
    public float AlertTimer;
    private int DifficultyNormal;
    private int DifficultyEasy;
    private int DifficultyHard;
    private int SelectedDiddiculty;
    public int difficulty;


    void Awake()
    {
        AlertTimer = PlayerPrefs.GetInt("DIFFICCULTYS");
        AlertOn.SetActive(false);
        AlertCaution.SetActive(false);
        Firstly = false;
        if (Instance != null && Instance != this)
        {
            DestroyImmediate(gameObject);
            return;
        }
        Instance = this;
        DontDestroyOnLoad(gameObject);
    }


    void Update () {
        if(AlertTimer <= 0)
        {
            ALERT = false;
            Firstly = false;
            AlertOn.SetActive(false);
            AlertOff.SetActive(true);
        }
        if (ALERT == true && Firstly == false)
        {
            AlertTimer = PlayerPrefs.GetInt("DIFFICCULTYS");
            AlertOn.SetActive(true);
            AlertOff.SetActive(false);
            SoundController.Instance.PlaySingle(alertsound);
            Firstly = true;
        }
        if (ALERT == true)
        {
            AlertTimer -= 1;
        }

        }
    public void StartButton()
    {
        Menu.SetActive(false);
        Diffi.SetActive(true);
    }
    public void QuitButton()
    {
        Application.Quit();
    }
    public void Restart()
    {
        Menu.SetActive(false);
        Load.SetActive(true);
        Application.LoadLevel(Application.loadedLevel);

    }

    public void difficultynormal()
    {
        SelectedDiddiculty = 999;
        PlayerPrefs.SetInt("DIFFICCULTYS", SelectedDiddiculty);
        Load.SetActive(true);
        Application.LoadLevel(1);
    }

    public void difficultyeasy()
    {
        SelectedDiddiculty = 555;
        PlayerPrefs.SetInt("DIFFICCULTYS", SelectedDiddiculty);
        Load.SetActive(true);
        Application.LoadLevel(1);
    }

    public void difficultyhard()
    {
        SelectedDiddiculty = 1555;
        PlayerPrefs.SetInt("DIFFICCULTYS", SelectedDiddiculty);
        Load.SetActive(true);
        Application.LoadLevel(1);
    }
}
