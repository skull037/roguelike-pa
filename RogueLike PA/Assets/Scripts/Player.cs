﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Player : MonoBehaviour
{

    public float speed = 6.0F;
    public float jumpSpeed = 8.0F;
    public float gravity = 20.0F;
    private int PlayerHealth = 100;
    public Text HealthText;
    private Vector3 moveDirection = Vector3.zero;
    public bool IsDead;
    public Animator animator;
    public AudioClip SwordAttack;
    public GameObject Load;
    public GameObject GameOverScreen;

    void start()
    {
        animator = GetComponent<Animator>();
    }
    void Update()
    {
        if (Input.GetButton("Restart"))
        {
            Time.timeScale = 1f;
            Load.SetActive(true);
            Application.LoadLevel(Application.loadedLevel);
        }
        if (Input.GetButton("Quit"))
        {
            Application.Quit();
        }
        if (Input.GetButton("Slow motion"))
        {
            Time.timeScale = 0.4f;
        }
        if (Input.GetButtonUp("Slow motion"))
        {
            Time.timeScale = 1f;
        }
        if (PlayerHealth <= 0)
        {
            GameOverScreen.SetActive(true);
        }
        HealthText.text = "Health: " + PlayerHealth;
        if ((Input.GetAxis("Horizontal") < -0.1))
        {
            animator.SetTrigger("RunLeft");
        }
        else if ((Input.GetAxis("Horizontal") > 0.1))
        {
            animator.SetTrigger("RunRight");
        }
        else if((Input.GetAxis("Horizontal") == 0))
        {
            animator.SetTrigger("StandSTill");
        }

        Vector3 pos = transform.position;
        pos.z = 0;
        transform.position = pos;

        CharacterController controller = GetComponent<CharacterController>();
        if (controller.isGrounded)

        {
            moveDirection = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
            moveDirection = transform.TransformDirection(moveDirection);
            moveDirection *= speed;
            if (Input.GetButton("Sprint"))
            { moveDirection *= 2; }
            if (Input.GetButton("Jump"))
            {
                moveDirection.y = jumpSpeed;
            }

        }
        moveDirection.y -= gravity * Time.deltaTime;
        controller.Move(moveDirection * Time.deltaTime);
    }
    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag == "Enemy")
        {
            SoundController.Instance.PlaySinglePitch(SwordAttack);
            animator.SetTrigger("Attacking");
        }
        if (col.gameObject.tag == "Enemy"&& GameController.Instance.ALERT == true)
        {
            SoundController.Instance.PlaySinglePitch(SwordAttack);
            animator.SetTrigger("Attacking");
            animator.SetTrigger("Damaged");
            PlayerHealth -= 50;
        }
        if (col.gameObject.tag == "HealthKit")
        {
            PlayerHealth += 25;
            Destroy(col.gameObject);

        }
        if (col.gameObject.tag == "dead")
        {
            PlayerHealth -= 100;

        }
        if (col.gameObject.tag == "LoadNextLevel")
        {
            Load.SetActive(true);
            int i = Application.loadedLevel;
            Application.LoadLevel(i + 1);
        }

    }
}
