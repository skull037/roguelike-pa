﻿using UnityEngine;
using System.Collections;

public class SoundController : MonoBehaviour {
    public static SoundController Instance;

    private float lowPitchRange = 0.95f;
    private float highPitchRange = 1.05f;

    public AudioSource soundEffect;
    // Use this for initialization
    void Awake() {
        if (Instance != null && Instance != this)
        {
            DestroyImmediate(gameObject);
            return;
        }
        Instance = this;
        DontDestroyOnLoad(gameObject);
    }

	
	// Update is called once per frame
	void Update () {
	
	}
    public void PlaySinglePitch(params AudioClip[] clips)
    {
        RandomSoundEffect(clips);
        soundEffect.Play();
    }
    public void PlaySingle(params AudioClip[] clips)
    {
        int randomSoundIndex = Random.Range(0, clips.Length);
        soundEffect.clip = clips[randomSoundIndex];
        soundEffect.Play();
    }
    private void RandomSoundEffect(AudioClip[] clips)
    {
        int randomSoundIndex = Random.Range(0, clips.Length);
        float randomPitch = Random.Range(lowPitchRange, highPitchRange);

        soundEffect.pitch = randomPitch;
        soundEffect.clip = clips[randomSoundIndex];
    }
}
